export const lists = {
    data: {
        items: [
            {id: 1, name: 'item 1', description: 'desc'},
            {id: 2, name: 'item 2', description: 'desc'},
            {id: 3, name: 'item 3', description: 'desc'},
            {id: 4, name: 'item 4', description: 'desc'},
            {id: 5, name: 'item 5', description: 'desc'},
            {id: 6, name: 'item 6', description: 'desc'},
            {id: 7, name: 'item 7', description: 'desc'},
            {id: 8, name: 'item 8', description: 'desc'},
            {id: 9, name: 'item 9', description: 'desc'},
            {id: 10, name: 'item 10', description: 'desc'}
        ],
        lists: [
            {id: 1, title: 'Category 1', parentId: null, itemsId: [1, 2, 3]},
            {id: 2, title: 'Category 2', parentId: 1, itemsId: [4, 5, 6]}
        ]

    }
};
