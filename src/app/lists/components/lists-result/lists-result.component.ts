import { Component, OnInit } from '@angular/core';
import {ListsQuery} from '../../state/lists.query';
import {Observable} from 'rxjs';
import {ItemsQuery} from '../../state/items.query';


@Component({
  selector: 'app-lists-result',
  templateUrl: './lists-result.component.html',
  styleUrls: ['./lists-result.component.styl']
})
export class ListsResultComponent implements OnInit {
  actives$ = new Observable<any>();

  constructor(private listsQuery: ListsQuery,
              private itemsQuery: ItemsQuery) { }

  ngOnInit(): void {
    this.actives$ = this.listsQuery.selectActiveId();
  }

  getItem(id: number) {
    return this.itemsQuery.getEntity(id);
  }

}
