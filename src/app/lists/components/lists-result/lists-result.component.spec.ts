import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListsResultComponent } from './lists-result.component';

describe('ListsResultComponent', () => {
  let component: ListsResultComponent;
  let fixture: ComponentFixture<ListsResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListsResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListsResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
