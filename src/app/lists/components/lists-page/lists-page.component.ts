import { Component, OnInit } from '@angular/core';
import {ListsService} from '../../state/lists.service';
import {List} from '../../state/list.model';
import {ItemsQuery} from '../../state/items.query';

@Component({
  selector: 'app-lists-page',
  templateUrl: './lists-page.component.html',
  styleUrls: ['./lists-page.component.styl']
})
export class ListsPageComponent implements OnInit {
  lists: any;

  constructor(private listsService: ListsService,
              private itemsQuery: ItemsQuery) { }

  ngOnInit(): void {
    this.listsService.getAll();
  }


}
