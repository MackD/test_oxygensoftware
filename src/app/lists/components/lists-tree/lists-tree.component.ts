import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {List} from '../../state/list.model';
import {ListsQuery} from '../../state/lists.query';
import {ListsService} from '../../state/lists.service';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {arrayFind, getEntityType, getIDType} from '@datorama/akita';
import {ID} from '@datorama/akita/lib/types';
import {ItemsQuery} from '../../state/items.query';
import {ListsState} from '../../state/lists.store';
import {async} from 'rxjs/internal/scheduler/async';
import {ItemsState} from '../../state/items.store';
import {ItemsService} from '../../state/items.service';
import {Item} from '../../state/item.model';

@Component({
    selector: 'app-lists-tree',
    templateUrl: './lists-tree.component.html',
    styleUrls: ['./lists-tree.component.styl']
})

export class ListsTreeComponent implements OnInit {
    selectLoading$: Observable<boolean>;
    lists$: Observable<List[]>;
    items$: Observable<Item[]>;
    treeList: any;

    constructor(private listsQuery: ListsQuery,
                private itemsQuery: ItemsQuery,
                private listsService: ListsService,
                private itemsService: ItemsService) {
    }

    ngOnInit() {
        this.selectLoading$ = this.listsQuery.selectLoading();
        this.lists$ = this.listsQuery.selectAll();
        this.items$ = this.itemsQuery.selectAll();
        this.listSub();
    }

    listSub() {
      this.lists$.subscribe(entities =>
        this.treeList = this.createTree(entities)
      );
    }

    getItem(id: number) {
        return this.itemsQuery.getEntity(id);
    }

    toggleActive(id: number) {
        this.listsService.toggleActive(id);
    }

    hasActive(id: number): boolean {
        return this.listsQuery.hasActive(id);
    }


    private createTree(items, id = null, link = 'parentId') {
        return items
            .filter(item => item[link] === id)
            .map(item => ({...item, children: this.createTree(items, item.id)}));
    }

}
