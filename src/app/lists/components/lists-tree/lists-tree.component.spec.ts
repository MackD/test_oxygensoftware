import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListsTreeComponent } from './lists-tree.component';

describe('ListsTreeComponent', () => {
  let component: ListsTreeComponent;
  let fixture: ComponentFixture<ListsTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListsTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListsTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
