import { Injectable } from '@angular/core';
import {arrayFind, ID} from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { ItemsStore } from './items.store';
import { Item } from './item.model';
import { tap } from 'rxjs/operators';
import {ItemsQuery} from './items.query';

@Injectable({ providedIn: 'root' })
export class ItemsService {

  constructor(private itemsStore: ItemsStore) {}


  add(item: Item) {
    this.itemsStore.add(item);
  }

  update(id, item: Partial<Item>) {
    this.itemsStore.update(id, item);
  }

  remove(id: ID) {
    this.itemsStore.remove(id);
  }

}
