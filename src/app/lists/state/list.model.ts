import {ID} from '@datorama/akita';
import {Item} from './item.model';

export interface List {
  id: number;
  title: string;
  parentId?: (List | ID);
  itemsId?: (Item | ID)[];
}

export function createList(params: Partial<List>) {
  return { } as List;
}
