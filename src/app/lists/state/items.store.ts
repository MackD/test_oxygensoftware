import { Injectable } from '@angular/core';
import { Item } from './item.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface ItemsState extends EntityState<Item> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'items' })
export class ItemsStore extends EntityStore<ItemsState> {

  constructor() {
    super();
  }

}

