import { Injectable } from '@angular/core';
import { List } from './list.model';
import {ActiveState, EntityState, EntityStore, StoreConfig} from '@datorama/akita';


export interface ListsState extends EntityState<List>, ActiveState {

}

const initialState = {
  active: []
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'lists' })
export class ListsStore extends EntityStore<ListsState> {

  constructor() {
    // @ts-ignore
    super(initialState);
  }

}

