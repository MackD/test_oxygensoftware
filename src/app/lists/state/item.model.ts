export interface Item {
  id: number | string;
  name: string;
  description: string;
}

export function createItem(params: Partial<Item>) {
  return { } as Item;
}
