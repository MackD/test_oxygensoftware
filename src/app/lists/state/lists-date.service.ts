import { Injectable } from '@angular/core';
import { timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { lists } from '../normalized';

@Injectable({
    providedIn: 'root'
})
export class ListsDataService {
    getLists() {
        return timer(1000).pipe(mapTo(lists));
    }
};