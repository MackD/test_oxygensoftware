import { Injectable } from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import { ListsStore, ListsState } from './lists.store';
import {combineLatest} from 'rxjs';
import {ItemsQuery} from './items.query';
import {map} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ListsQuery extends QueryEntity<ListsState> {

  constructor(protected store: ListsStore,
              private itemsQuery: ItemsQuery) {
    super(store);
  }
}

