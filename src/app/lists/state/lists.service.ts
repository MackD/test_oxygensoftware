import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { ListsStore } from './lists.store';
import { List } from './list.model';
import {ListsDataService} from './lists-date.service';
import {ItemsStore} from './items.store';
import * as storage from '../storage';
import {ListsQuery} from './lists.query';

@Injectable({ providedIn: 'root' })
export class ListsService {

  constructor(private listsStore: ListsStore,
              private listsQuery: ListsQuery,
              private itemsStore: ItemsStore,
              private listsDataService: ListsDataService) {}

  // get() {
  //     this.listsStore.set(DATA);
  // }

  async getAll() {
      const response = await this.listsDataService.getLists().toPromise();

      const allItems = response.data.items;
      const lists = response.data.lists;

      this.listsStore.set(lists);
      this.itemsStore.set(allItems);
      // @ts-ignore
      this.listsStore.setActive(this.getActiveLocal());
  }

  add(list: List) {
    this.listsStore.add(list);
  }

  update(id, list: Partial<List>) {
    this.listsStore.update(id, list);
  }

  remove(id: ID) {
    this.listsStore.remove(id);
  }

  toggleActive(id: number) {
    this.listsStore.toggleActive(id);
    this.saveActiveLocal();
  }

  saveActiveLocal() {
      const activeId = this.listsQuery.getActiveId();
      storage.saveSession(activeId);
  }

  getActiveLocal() {
      const activeId = storage.getSession();
      return activeId;
  }

}
