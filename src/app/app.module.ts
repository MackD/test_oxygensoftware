import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { environment } from '../environments/environment';
import { ListsResultComponent } from './lists/components/lists-result/lists-result.component';
import { ListsTreeComponent } from './lists/components/lists-tree/lists-tree.component';
import { ListsPageComponent } from './lists/components/lists-page/lists-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatListModule} from '@angular/material/list';

@NgModule({
  declarations: [
    AppComponent,
    ListsResultComponent,
    ListsTreeComponent,
    ListsPageComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        environment.production ? [] : AkitaNgDevtools,
        AkitaNgRouterStoreModule,
        BrowserAnimationsModule,
        MatTreeModule,
        MatIconModule,
        MatButtonModule,
        MatCheckboxModule,
        NgbModule,
        MatListModule
    ],
  providers: [{ provide: NG_ENTITY_SERVICE_CONFIG, useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' }}],
  bootstrap: [AppComponent]
})
export class AppModule { }
